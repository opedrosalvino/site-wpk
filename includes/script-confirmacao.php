<?php
session_start();
if(is_null($_SESSION['logado']) || empty($_SESSION['logado']) || $_SESSION['logado'] != true){
    header('Location: ../index.php');
}
if(!is_null($_SESSION['user']) && !empty($_SESSION['user'])){
    $usuario = $_SESSION['user'];
}else{
    $usuario = null;
}
if(!is_null($_SESSION['codigo']) && !empty($_SESSION['codigo'])){
    $codigo = $_SESSION['codigo'];
}else{
    $codigo = null;
}
if(!is_null($_POST['codigo_ver']) && !empty($_POST['codigo_ver'])){
    $codigo_ver = $_POST['codigo_ver'];
}else{
    $codigo_ver = null;
}

if(!is_null($usuario) && !is_null($codigo) && !is_null($codigo_ver)){
    if($codigo_ver == $codigo){
        echo "<script>confirm('Login efetuado com sucesso!',window.location.href='../usuario.php')</script>";
    }
    else{
        echo "<script>confirm('Código incorreto. Digite novamente',window.location.href='confirmacao.php')</script>";
    }
}
?>