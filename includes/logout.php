<?php
    $_SESSION["logado"] = false;
    $_SESSION["user"] = null;
    $_SESSION["codigo"] = null;
    session_destroy();
    header("Location: ../index.php");
?>