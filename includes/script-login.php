<?php
session_start();
//Defino a Chave do meu site
$secret_key = '6LdJe-gaAAAAAGxCNFiCpx98K-12e3TlSnMWgvSO';

//Pego a validação do Captcha feita pelo usuário
$recaptcha_response = $_POST['g-recaptcha-response'];

if(!is_null($_POST['email_user']) && !empty($_POST['email_user'])
&& !is_null($_POST['senha_user']) && !empty($_POST['senha_user'])){

    // Verifico se foi feita a postagem do Captcha 
    if(isset($recaptcha_response)){
            
        // Valido se a ação do usuário foi correta junto ao google
        $answer = 
            json_decode(
                file_get_contents(
                    'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.
                    '&response='.$_POST['g-recaptcha-response']
                )
            );

        // Se a ação do usuário foi correta executo o restante do meu formulário
        if($answer->success) {
            if($_POST['senha_user'] == 'teste@1235' || $_POST['senha_user'] == 'teste@3587'){
                $_SESSION['user']=$_POST['email_user'];
                $_SESSION['logado']=true;
                header('Location: verificacao.php');
            }
            else{
                echo "<script>confirm('Conta não encontrada!',window.location.href='../index.php')</script>";
            }
        }
        else {
            echo "<script>confirm('Clique na verificação do captcha',window.location.href='../index.php')</script>";
        }
    }
    else{
        echo "<script>confirm('Captcha não verificado',window.location.href='../index.php')</script>";
    }
}
else{
    echo "<script>confirm('Dados inválidos',window.location.href='../index.php')</script>";
}