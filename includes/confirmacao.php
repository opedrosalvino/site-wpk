<?php
session_start();
if(is_null($_SESSION['logado']) || empty($_SESSION['logado']) || $_SESSION['logado'] != true){
    header('Location: ../index.php');
}
if(!is_null($_SESSION['user']) && !empty($_SESSION['user'])){
    $usuario = $_SESSION['user'];
}else{
    $usuario = null;
}
if(!is_null($_SESSION['codigo']) && !empty($_SESSION['codigo'])){
    $codigo = $_SESSION['codigo'];
}else{
    $codigo = null;
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>WPK | Confirmação</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/main.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
    <header class="bg-escuro">
        <div class="text-center">
            <img class="logo" src="../assets/logo.png" alt="">
        </div>
    </header>
    <main class="container">
        <div class="row">
            <div class="col-md-8 mx-auto my-5 py-4 px-4 bg-verde card">
                <h1 class="h3 text-center text-light mb-3">Digite o código de confirmação</h1>
                <h2><?php echo $codigo;?></h2>
                <form action="script-confirmacao.php" method="POST">
                    <div class="form-group">
                        <label class="h5 text-light" for="codigo-ver">Código Numérico</label>
                        <input class="form-control" type="text" name="codigo_ver" id="codigo-ver" required minlength="1">
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="col-md-8 mx-auto">
                            <button class="btn btn-dark btn-block">Confirmar</button>
                        </div>
                    </div>
                </form>
            </div>    
        </div>
    </main>
</body>
</html>