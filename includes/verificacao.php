<?php
session_start();
if(is_null($_SESSION['logado']) || empty($_SESSION['logado']) || $_SESSION['logado'] != true){
    header('Location: ../index.php');
}
if(!is_null($_SESSION['user']) && !empty($_SESSION['user'])){
    $destino = $_SESSION['user'];
}
else{
    $destino = null;
}

$data_atual = date('d/m/Y');
$hora_atual = date('H:i:m');

if(!is_null($destino)){
    $codigo = rand(198919,988416);
    $corpo = '
    <!DOCTYPE html>
    <html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>Envio do código verificador</title>
    </head>
    <body>
        <h1>Seu código de verificação é: '.$codigo. '.<br>Insira esse código para continuar a utilizar a plataforma.</h1>
    </body>
    </html>
    ';
    $assunto = 'Código de verificação - Site WPK';
    $headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'From: Pedro Souza <pedro.luiz7199@gmail.com>';

    $envio = mail($destino, $assunto, $corpo, $headers);

    if($envio){
    	$_SESSION['codigo']=$codigo;
        echo "<script>confirm('Código enviado! Verifique seu e-mail',window.location.href='confirmacao.php')</script>";
    }
    else{
    	echo "<script>confirm('Houve um erro',window.location.href='../index.php')</script>";
    }
}
else{
    echo "<script>confirm('Usuário não encontrado',window.location.href='../index.php')</script>";
}
?>