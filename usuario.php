<?php
session_start();
if(!is_null($_SESSION['user']) && !empty($_SESSION['user'])){
    $usuario = $_SESSION['user'];
}else{
    $usuario = null;
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>WPK | Usuário</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
    <header class="bg-escuro">
        <nav class="navbar navbar-expand-lg navbar-dark bg-escuro">
            <a class="navbar-brand" href="#">
                <img class="logo" src="assets/logo.png" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                <li class="nav-item"></li>
                    <li class="nav-item"><a class="nav-link" href="#"><?php if(!is_null($usuario)){echo $usuario;}else{echo "Usuário";}?></a></li>
                    <li class="nav-item"><a class="nav-link" href="includes/logout.php">Sair</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <main class="container">
        <div class="row">
            <div class="col-md-8 mx-auto my-5 bg-verde py-5 card">
                <h1 class="h4 text-center mb-3">Seja bem-vindo(a) <?php if(!is_null($usuario)){echo $usuario;}else{echo "Usuário";}?></h1>
                
            </div>    
        </div>
    </main>
</body>
</html>