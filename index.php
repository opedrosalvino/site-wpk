<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>WPK - Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
    <header class="bg-escuro">
        <div class="text-center">
            <img class="logo" src="assets/logo.png" alt="">
        </div>
    </header>
    <main class="container">
        <div class="row">
            <div class="col-md-8 mx-auto my-5 py-3 bg-gradient card">
                <h1 class="h3 text-center text-light mb-3">Entre na sua conta</h1>
                <form action="includes/script-login.php" method="POST">
                    <div class="form-group">
                        <label class="h5 text-light" for="email-user">E-mail</label>
                        <input class="form-control" type="email" name="email_user" id="email-user" required maxlength="45" minlength="5">
                    </div>
                    <div class="form-group">
                        <label class="h5 text-light" for="senha-user">Senha</label>
                        <input class="form-control" type="password" name="senha_user" id="senha-user" required maxlength="15" minlength="8">
                    </div>
                    <div class="form-row">
                        <div class="col-md-8 mx-auto">
                            <div class="row my-2">
                                <div class="col-md-8 mx-auto">
                                    <div class="g-recaptcha" data-sitekey="6LdJe-gaAAAAAOxUfbXzcBCxscKmrFUvCB11OFht"></div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <button class="btn btn-dark btn-block">Entrar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>    
        </div>
    </main>
</body>
</html>